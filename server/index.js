const express = require('express')
const http = require('http')
const hat = require('hat')
const url = require('url')
const gcm = require('node-gcm');
const WebSocket = require('ws')
const ChatController = require('./ChatController')
const UserController = require('./UserController')

const app = express()
const PORT = 1337
const HOST = '0.0.0.0'

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.get('/users/:id', async (req, res) => {
    UserController.updateSeen(req.params.id, 'user')
    res.json({status: 'success'})
})
app.get('/users', async (req, res) => {
    try {
        const users = await UserController.getUsers()
        res.json({status: 'success', users: users})
    } catch(e) {
        res.json({status: 'failed'})
    }
})
app.get('/users/:id/conversations', async (req, res) => {
    try {
        const conversations = await UserController.getMessages(req.params.id)
        res.json({status: 'success', conversations: conversations})
    } catch(e) {
        res.json({status: 'failed'})
    }
})
app.get('/sendNotification', async (req, res) => {

    function sendNotification(title, msg) {
        const message = new gcm.Message({
            collapseKey: 'demo',
            priority: 'high',
            contentAvailable: true,
            delayWhileIdle: true,
            timeToLive: 3,
            restrictedPackageName: "com.codity.chatadmin",
            dryRun: false,
            data: {
                key1: 'message1',
                key2: 'message2'
            },
            notification: {
                title: 'nice',
                icon: "ic_launcher",
                body: 'test'
            }
        })
        const sender = new gcm.Sender(process.env.gcm_token);
        const registrationTokens = [];
        registrationTokens.push(process.env.reg_token);

        sender.sendNoRetry(message, {registrationTokens: registrationTokens}, function (err, response) {
            if (err) console.error('error', err);
            else console.log(response);
        });
    }


// ... or some given values
    sendNotification();
    res.json({status: 'sent'})

})
app.delete('/users/:user', (req, res) => {
    UserController.removeUser(req.params.user)
    res.send({status: 'success'})
})

const server = http.createServer(app)
const wss = new WebSocket.Server({ server })

wss.on('connection', (ws, req) => {
    const key = hat()
    let userKey = ChatController.addClient(key, ws, req.connection.remoteAddress)
    ws.on('message', (message) => {
        message = JSON.parse(message)
        if (message.type === 'auth') {
            ChatController.authenticate(userKey, message).then(function(v) {
                userKey = v;
            });
        } else {
            ChatController.processMessage(userKey, message)
        }
    })
    ws.on('error', () => {
        console.log('an error occured, host might have been abruptly closed the connection...')
    })
    ws.on('close', () => {
        ChatController.removeUser(userKey)
    })
})
server.listen(PORT, HOST, () => {
    console.log('Listening on %d', server.address().port);
});

