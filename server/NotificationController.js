const gcm = require('node-gcm');


module.exports = {
    sendNotification(title, msg, userId) {
        const message = new gcm.Message({
            collapseKey: 'demo',
            priority: 'high',
            contentAvailable: true,
            delayWhileIdle: true,
            timeToLive: 3,
            restrictedPackageName: "com.codity.chatadmin",
            dryRun: false,
            data: {
                id: userId,
                key2: 'message2'
            },
            notification: {
                title: title,
                icon: "ic_launcher",
                body: msg,
                sound: 'default'
            }
        })

        const sender = new gcm.Sender(process.env.gcm_token);

        const registrationTokens = [];
        registrationTokens.push(process.env.reg_token);

        sender.sendNoRetry(message, {registrationTokens: registrationTokens}, function (err, response) {
            if (err) console.error('error', err);
            else console.log('sent successfully');
        });
    }
}