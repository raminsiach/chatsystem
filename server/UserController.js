const AWS = require('aws-sdk')
const ObjectId = require('node-time-uuid')

const production = process.env.NODE_ENV === 'production'
let awsConfig = {
    region: "us-west-2",
    endpoint: "http://localhost:8000"
}

if (production) {
    awsConfig.region = "us-east-1"
    awsConfig.endpoint = 'https://dynamodb.us-east-1.amazonaws.com'
    awsConfig.accessKeyId = process.env.accessKeyId
    awsConfig.secretAccessKey = process.env.secretAccessKey
}
console.log(process.env.NODE_ENV)
AWS.config.update(awsConfig)

const docClient = new AWS.DynamoDB.DocumentClient()

module.exports = {
    getUsers() {
        const params = {
            TableName: 'Users'
        }
        return new Promise((resolve, reject) => {
            docClient.scan(params, (err, data) => {
                if (err) {
                    reject(data)
                }
                if (data && data.Items) {
                    resolve(data.Items)
                } else {
                    return []
                }

            })
        })

    },
    getMessages(userId) {
        const params = {
            TableName: 'Conversations',
            KeyConditionExpression: 'u_id = :id',
            ExpressionAttributeValues: {
                ':id': userId
            }
        }
        return new Promise((resolve, reject) => {
            docClient.query(params, (err, data) => {
                if (err) {
                    reject(data)
                } else {
                    resolve(data.Items)
                }
            })
        })

    },
    updateSeen(id, from_u) {
        console.log(id, from_u)
        const params = {
            TableName: 'Conversations',
            IndexName: 'fromIndex',
            KeyConditionExpression: 'u_id = :id AND from_u = :fromu',
            ExpressionAttributeValues: {
                ':id': id,
                ':fromu': from_u
            }
        }
        docClient.query(params, (err, data) => {
            if (err) {
                console.log('err', err)
            } else {
                // update unseen on Users table
                const params = {
                    TableName: 'Users',
                    Key: {
                        id: id
                    },
                    UpdateExpression: "set unseen_docs = :val",
                    ExpressionAttributeValues: {
                        ':val': 0
                    },
                    ReturnValues: 'UPDATED_NEW'
                }
                docClient.update(params, (err, data) => {
                    if (err) {
                        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                    } else {
                        console.log("UpdateItem succeeded:");
                    }
                })
                //update conversations
                data.Items.forEach((item) => {
                    const params = {
                        TableName: 'Conversations',
                        Key:{
                            "u_id": item.u_id,
                            "id": item.id
                        },
                        UpdateExpression: "set seen = :s",
                        ExpressionAttributeValues: {
                            ':s': 1
                        },
                        ReturnValues: 'UPDATED_NEW'
                    }

                    docClient.update(params, (err, data) => {
                        if (err) {
                            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                        } else {
                            console.log('updated')
                        }
                    })
                })
            }
        });
    },
    removeUser(id) {
        const params = {
            TableName: 'Users',
            Key:{
                "id": id
            }
        }
        docClient.delete(params, (err, data) => {
            if (err) {
                console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                const params = {
                    TableName: 'Conversations',
                    KeyConditionExpression: 'u_id = :id',
                    ExpressionAttributeValues: {
                        ':id': id
                    }
                }
                docClient.query(params, (err, data) => {
                    if (err) {
                        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                    } else {
                        data.Items.forEach((item) => {
                            const params = {
                                TableName: 'Conversations',
                                Key:{
                                    "u_id": item.u_id,
                                    "id": item.id
                                }
                            }
                            docClient.delete(params, (err, data) => {
                                if (err) {
                                    console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                                } else {
                                }
                            })
                        })
                    }
                });

            }
        });
    }
}