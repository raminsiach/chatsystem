"use strict";
const AWS = require('aws-sdk')
const ObjectId = require('node-time-uuid')
const fetch = require("node-fetch");
const UserController = require('./UserController')
const NotificationController = require('./NotificationController')

const production = process.env.NODE_ENV === 'production'

let awsConfig = {
    region: "us-west-2",
    endpoint: "http://localhost:8000"

}

if (production) {
    awsConfig.region = "us-east-1"
    awsConfig.endpoint = 'https://dynamodb.us-east-1.amazonaws.com'
    awsConfig.accessKeyId = process.env.accessKeyId
    awsConfig.secretAccessKey = process.env.secretAccessKey
}

AWS.config.update(awsConfig)

const docClient = new AWS.DynamoDB.DocumentClient()

module.exports = {
    clients: new Map(),
    connections: new Map(),
    admin: null,
    adminEmail: 'codity@codity.io',
    clientMessages: new Map(),
    welcomeMessage: 'Hi, Welcome to codity, Ask me any questions you have...',
    addClient(key, connection, ipAddress) {
        this.clients.set(key, {type: 'user', connection: connection, ipAddress: ipAddress})
        return key
    },
    updateClientMessages(userKey, userType, message) {
        let currentClientMessages = this.clientMessages.get(userKey)
        if (currentClientMessages == null)
            currentClientMessages = []
        currentClientMessages.push({from_u: userType, msg: message.msg, dt: message.dt})
        this.clientMessages.set(userKey, currentClientMessages)
    },
    async authenticate(userKey, message) {
        let key;
        if (message.msg) {
            key = message.msg
            const client = this.clients.get(userKey)
            this.clients.delete(userKey)
            this.clients.set(key, client)
            let json;
            let endpoint = 'http://freegeoip.net/json/' + this.clients.get(key).ipAddress

            try {
                const response = await fetch(endpoint);
                json = await response.json();
                this.updateOnlineStatus(key, 1, (json.region_name + ', ' + json.country_name));
                this.sendWelcomeMessage(key)
                const params = {
                    TableName : "Conversations",
                    KeyConditionExpression: "u_id = :k",
                    ExpressionAttributeValues: {
                        ":k": key
                    }
                };
                docClient.query(params, (err, data) => {
                    if (err) {
                        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                    } else {
                        data.Items.forEach((item) => {
                            this.updateClientMessages(item.u_id, item.from_u, {msg: item.msg, dt: item.dt})
                            this.clients.get(item.u_id).connection.send(JSON.stringify({from: item.from_u, msg: item.msg}))
                        });
                        if (this.admin != null) {
                            this.admin.send(JSON.stringify({type: 'messages', msg: [...this.clientMessages]}))
                        }
                    }
                });
            } catch (e) {
                console.log(e)
            }
        } else {
            key = userKey
            // save user to db
            let endpoint = 'http://freegeoip.net/json/' + this.clients.get(key).ipAddress
            let json;
            try {
                const response = await fetch(endpoint);
                json = await response.json();
                const params = {
                    TableName: 'Users',
                    Item: {
                        id: key,
                        u_type: 'anonym',
                        last_online: Date.now(),
                        u_online: 1,
                        unseen_docs: 0,
                        u_region: (json.region_name + ', ' + json.country_name)

                    }
                }
                docClient.put(params, (err, data) => {
                    if (err) {
                        console.error("Unable to add user", "Error JSON:", JSON.stringify(err, null, 2));
                    } else {
                        this.clients.get(key).connection.send(JSON.stringify({type: 'auth', id: key}))
                        this.updateAdminOnNewUsers()
                        this.sendWelcomeMessage(key)
                    }
                });
            } catch (e) {
                console.log(e)
            }
        }
        return key
    },
    processMessage(userKey, message) {
        console.log(userKey);
        const now = Date.now()
        if (message.type === 'msg') {
            let params = {
                TableName: 'Conversations',
                Item: {
                    id: new ObjectId().toString(),
                    dt: now,
                    seen: 0
                }
            }
            if (message.from === 'user') {
                if (this.clientMessages.has(userKey)) {
                    message.dt = now
                    this.updateClientMessages(userKey, 'user', message)
                } else {
                    this.clientMessages.set(userKey, [{from_u: 'user', msg: message.msg, dt: now}])
                }
                // save incoming conversations from user to Conversations table
                params.Item.from_u = 'user'
                NotificationController.sendNotification('Msg from ' + userKey.substring(0, 6),message.msg, userKey )
            } else if (message.from === 'admin') {
                userKey = message.client
                message.dt = now
                this.updateClientMessages(message.client, 'admin', message)
                params.Item.from_u = 'admin'
                this.clients.get(message.client).connection.send(JSON.stringify({from: 'admin', msg: message.msg, dt: now}))
                this.admin.send(JSON.stringify({type: 'messages', msg: Array.from(this.clientMessages)}))
            }
            params.Item.u_id = userKey
            params.Item.msg = message.msg
            docClient.put(params, (err, data) => {
                if (err) {
                    console.error("Unable to add user", "Error JSON:", JSON.stringify(err, null, 2));
                } else {
                    const params = {
                        TableName: 'Users',
                        Key:{
                            id: userKey
                        },
                        UpdateExpression: "set unseen_docs = unseen_docs + :val",
                        ExpressionAttributeValues:{
                            ":val":1
                        },
                        ReturnValues:"UPDATED_NEW"
                    }
                    docClient.update(params, (err, data) => {
                        if (err) {
                            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                        } else {
                            if (message.from === 'user') {
                                if (this.admin != null) {
                                    this.admin.send(JSON.stringify({type: 'messages', msg: [...this.clientMessages]}))
                                    UserController.getUsers().then(data => {
                                        this.admin.send(JSON.stringify({type: 'clients', msg: data}))
                                    })
                                } else {
                                    this.clients.get(userKey).connection.send(JSON.stringify({from: 'admin', msg: 'We are not online right now. In case of no answer in the next minute or so, please send your question to: ' + this.adminEmail}))
                                }
                            }

                        }
                    });

                }
            });

        } else if (message.type === 'identity') {
            if (message.from === 'admin') {
                let admin = this.clients.get(userKey)
                admin.type = 'admin'
                this.clients.set(userKey, admin)
                this.admin = admin.connection
                if (this.admin != null) { // is this check necessary
                    if (this.clients.size > 1) { // Either 1 user joined before admin or its only admin that joined. therefore to send this info it should be 2 users
                        // this.admin.send(JSON.stringify({type: 'clients', msg: [...this.clients].map((item) => ({index: item[0], type: item[1].type}))}))
                        UserController.getUsers().then(data => {
                            this.admin.send(JSON.stringify({type: 'clients', msg: data}))
                        })
                        this.admin.send(JSON.stringify({type: 'messages', msg: [...this.clientMessages]}))
                        this.broadcastAdminStatus('online')
                    }
                }



            }
        } else if (message.type === 'seen') {
            console.log('modfiying seen conversations')
            const key = (message.from === 'admin') ? message.client : userKey
            this.updateSeenConversations('user', key)
            console.log('key', key)
        } else if (message.type === 'debug') {
            console.log(message.msg)
        }
    },
    sendWelcomeMessage(userKey) {
        this.clients.get(userKey).connection.send(JSON.stringify({from: 'admin', msg: this.welcomeMessage}))
        if (this.admin != null) {
            this.adminIsOnline(userKey, 'online')
        } else {
            this.adminIsOnline(userKey, 'offline')
        }
    },
    updateAdminOnNewUsers() {
        if (this.admin != null) {
            // this.admin.send(JSON.stringify({type: 'clients', msg: [...this.clients].map((item) => (item[0]))}))
            UserController.getUsers().then(data => {
                this.admin.send(JSON.stringify({type: 'clients', msg: data}))
            })
        }
    },
    updateSeenConversations(from, userKey) {
        console.log('updating ' + userKey + ' updating seen for conversations that has from:' + from)
        UserController.updateSeen(userKey, from)
    },
    adminIsOnline(userKey, status) {
        this.clients.get(userKey).connection.send(JSON.stringify({type: 'status', status: status}))
    },
    broadcastAdminStatus(status) {
        this.clients.forEach((value, key, map) => {
            if (value.type !== 'admin') {
                this.adminIsOnline(key, status)
            }
        })
    },
    updateOnlineStatus: function (userKey, onlineStatus, region) {
        const expressionAttributes = {
            ':o': onlineStatus,
            ':l': Date.now()
        }
        if (region != undefined) {
            expressionAttributes[':r'] = region;
        }
        const params = {
            TableName: 'Users',
            Key: {
                id: userKey
            },
            UpdateExpression: "set u_online = :o, last_online = :l" + ((region != undefined) ? " , u_region = :r" : ""),
            ExpressionAttributeValues: expressionAttributes,
            ReturnValues: 'UPDATED_NEW'
        }
        docClient.update(params, (err, data) => {
            if (err) {
                console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                this.updateAdminOnNewUsers()
                console.log("UpdateItem succeeded:");
            }
        })
    },
    removeUser(userKey) {
        if (this.clients.get(userKey).type === 'admin') {
            this.admin = null;
            this.broadcastAdminStatus('offline')
        } else {
            this.updateOnlineStatus(userKey, 0);
        }
        this.clients.delete(userKey)
        this.clientMessages.delete(userKey)
        if (this.admin != null) {
            UserController.getUsers().then(data => {
                this.admin.send(JSON.stringify({type: 'clients', msg: data}))
            })
            // this.admin.send(JSON.stringify({type: 'clients', msg: )
            // this.admin.send(JSON.stringify({type: 'messages', msg: Array.from(this.clientMessages)}))
        }
    }
}