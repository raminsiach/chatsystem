import React from 'react'
import ReactDom from 'react-dom'
import ChatClient from './components/ChatClient'
import uniqueid from 'uniqueid'

let NsConfig = null

export default {
    config: (config) => {
        NsConfig = config
    },
    widget: {
        new: (config) => {
            let uid = uniqueid({prefix: 'widget_ns_'})
            return {
                render(args) {
                    ReactDom.render(<ChatClient
                        with="Codity"
                        pos={args.pos || config.pos || ''}
                        apiKey={args.apiKey || config.apiKey || NsConfig.apiKey}
                    />, document.querySelector(config.selector))
                }
            }
        }
    }
}
// ReactDom.render(<ChatClient/>, document.getElementById('codity-chat-app'))