import React from 'react'
import ChatMessages from './ChatMessages'
import ChatInput from './ChatInput'
import './ChatClient.css'
import Linkify from 'linkifyjs/react'
import cookie from 'react-cookies'
import Ionicon from 'react-ionicons'
class ChatClient extends React.Component {
    constructor(props) {
        super(props)
        this.userApiKey = '1c3509d618bd6f9489c92fd47aa1e830'
        this.userId = null;
        this.init = false;
        this.state = {
            chatOpen: false,
            messages: [],
            adminStatus: 'offline'
        }
        this.socket = null;
        this.openChatBox = this.openChatBox.bind(this)
        this.sendClientMessage = this.sendClientMessage.bind(this)
        this.sendMessage = this.sendMessage.bind(this)
    }
    componentDidMount() {
        if (this.socket) {

        }
    }
    render() {
        if (this.props.apiKey !== this.userApiKey)
            return null;
        let messageItems = this.state.messages.map((item, index) => {
            const type = (item.from === 'admin') ? 'server' : 'client'
            const classes = `chat-txt ${type}`

            return (
                <div className="codity-chat-block" key={index}>
                    <div className={classes}>
                        <Linkify tagName="div">{item.message}</Linkify>
                    </div>

                </div>)
        })
        const position = (this.props.pos && this.props.pos == 'left') ? 'left' : ''
        const buttonClasses = `codity_io--chat-btn ${position}`
        return (
            <div>
                <div className={buttonClasses} onClick={this.openChatBox}>
                    <span className={this.state.chatOpen ? '' : 'close-me'}>
                        <Ionicon icon="md-close" fontSize="35px" onClick={() => console.log('Hi!')} color="white"/>
                    </span>
                    <span className={this.state.chatOpen ? 'close-me' : ''}>
                        <Ionicon icon="md-chatbubbles" fontSize="35px" onClick={() => console.log('Hi!')} color="white"/>
                    </span>
                </div>
                <div id="codity_io--chat-box" className={this.state.chatOpen ? 'open' : ''}>
                    <div className="header">
                        Chatting with {this.props.with}
                        <div className="waiting">Usually answering in less than 30 mins</div>
                    </div>
                    <ChatInput inputRef={el => this.childContent = el} sendMessage={this.sendMessage} messages={messageItems} onKeyPress={this.sendClientMessage}/>

                </div>
            </div>
        )
    }
    updateChatBox(from, msg) {
        this.setState(prevState => {
            prevState.messages.push({from: from, message: msg})
            return {messages: prevState.messages}
        })
        setTimeout(() => {
            if (this.childContent) {
                this.childContent.scrollTop = this.childContent.scrollHeight
            }

        })
    }
    sendClientMessage(e) {
        if (e.key === 'Enter') {
            this.sendMessage(e.target);
            e.target.value = ''
        }
    }

    sendMessage(el) {
        const value = el.value
        if (value.length) {
            this.updateChatBox('client', value)
            this.socket.send(JSON.stringify({type: 'msg', from: 'user', msg: value, id: this.userId}))
            el.value = ''
        }
    }

    openChatBox(e) {
        if (!this.init) {
            if (this.props.apiKey == this.userApiKey)
                this.socket = new WebSocket(process.env.REACT_APP_BASE_URI)
            if (this.socket) {
                this.socket.addEventListener('open', (e) => {
                    let msg = {type: 'auth'}
                    this.userId = cookie.load('userId')
                    if (this.userId != null) {
                        msg.msg = this.userId
                    }
                    this.socket.send(JSON.stringify(msg))
                })
                this.socket.addEventListener('message', (e) => {
                    const data = JSON.parse(e.data)
                    if (data.type && data.type === 'status') {
                        this.setState(prev => ({
                            adminStatus: data.status
                        }))
                    } else if (data.type && data.type === 'auth') {
                        cookie.save('userId', data.id, { path: '/' })
                    } else {
                        this.updateChatBox(data.from, data.msg)
                    }
                })
            }
            this.init = true;
        }
        this.setState(prevState => (
            {chatOpen: !prevState.chatOpen}
        ))
    }
}

export default ChatClient