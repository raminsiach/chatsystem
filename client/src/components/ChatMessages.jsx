import React from 'react'

class ChatMessages extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                {this.props.messages}
            </div>
        )
    }
}

export default ChatMessages