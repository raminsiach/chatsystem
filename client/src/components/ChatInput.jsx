import React from 'react'
import ChatMessages from './ChatMessages'
import './ChatInput.css'
import Ionicon from 'react-ionicons'
class ChatInput extends React.Component {

    scrollOnEnter(e) {
        this.props.onKeyPress(e);
    }
    sendMessage() {
        console.log('value to send', this.input.value)
        this.props.sendMessage(this.input)
    }
    render() {
        return (
            <div className="chat-input">
                <div className="content" ref={this.props.inputRef}>
                    <ChatMessages messages={this.props.messages} />
                </div>

                <div className="actions">
                    <div className="input-message">
                        <input ref={el => this.input = el} type="text" placeholder="type here and press enter ..." onKeyPress={(e) => this.scrollOnEnter(e)}/>
                    </div>
                    <div className="send-message">
                        <Ionicon icon="md-send" fontSize="26px" title="send" color="#666" onClick={() => this.sendMessage()}/>
                    </div>


                </div>
            </div>
        )
    }
}
export default ChatInput