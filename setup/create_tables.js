var AWS = require("aws-sdk");

const production = process.env.NODE_ENV === 'production'
let awsConfig = {
    region: "us-west-2",
    endpoint: "http://localhost:8000"
}

if (production) {
    awsConfig.region = "us-east-1"
    awsConfig.accessKeyId = process.env.accessKeyId
    awsConfig.secretAccessKey = process.env.secretAccessKey
    awsConfig.endpoint = 'https://dynamodb.us-east-1.amazonaws.com'
}

AWS.config.update(awsConfig)

var dynamodb = new AWS.DynamoDB();

var dbUserParams = {
    TableName : "Users",
    KeySchema: [
        { AttributeName: "id", KeyType: "HASH"},  //Partition key
    ],
    AttributeDefinitions: [
        { AttributeName: "id", AttributeType: "S" }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    }
};

dynamodb.createTable(dbUserParams, function(err, data) {
    if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});
var dbConversationsParam = {
    TableName : "Conversations",
    KeySchema: [
        { AttributeName: "u_id", KeyType: "HASH"},  //Partition key
        { AttributeName: "id", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "id", AttributeType: "S" },
        { AttributeName: "u_id", AttributeType: "S"},
        { AttributeName: "from_u", AttributeType: "S"},
    ],
    GlobalSecondaryIndexes: [
        {
            IndexName: "fromIndex",
            KeySchema: [
                {AttributeName: "u_id", KeyType: "HASH"},
                {AttributeName: "from_u", KeyType: "RANGE"},

            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 5,
                WriteCapacityUnits: 5
            }
        }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    }
};

dynamodb.createTable(dbConversationsParam, function(err, data) {
    if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});