var AWS = require("aws-sdk");

const production = process.env.NODE_ENV === 'production'
let awsConfig = {
    region: "us-west-2",
    endpoint: "http://localhost:8000"
}

if (production) {
    awsConfig.region = "us-east-1"
    awsConfig.accessKeyId = process.env.accessKeyId
    awsConfig.secretAccessKey = process.env.secretAccessKey
    awsConfig.endpoint = 'https://dynamodb.us-east-1.amazonaws.com'
}

AWS.config.update(awsConfig)

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName : "Conversations"
};

dynamodb.deleteTable(params, function(err, data) {
    if (err) {
        console.error("Unable to delete table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Deleted table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});

var params = {
    TableName : "Users"
};

dynamodb.deleteTable(params, function(err, data) {
    if (err) {
        console.error("Unable to delete table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Deleted table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});
